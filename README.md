# 242-nuxt-template

This is a augmented vue-nuxt template made to ease the setup of nuxt development.

# Plugins

1. Vuetify (for admin-panel components)
2. Vuex (for persisted state store)
3. Vuelidate (for form validation)
4. Foundation (for grid, front-end components)

# How To Run

## vue init bitbucket:242/nuxt-custom-template .

